# Projects

Here is a list of projects that will be submitted to the OSPP,
containing the information required by this program.

## Porting KDE Games to Android platform

Name: **Porting KDE Games to Android platform**

Repository:

- https://invent.kde.org/games/kajongg.git
- https://invent.kde.org/games/kmahjongg.git
- https://invent.kde.org/games/kshisen.git
- https://invent.kde.org/games/kigo.git
- https://invent.kde.org/education/blinken.git
- https://invent.kde.org/games/klickety.git
- https://invent.kde.org/games/skladnik.git
- https://invent.kde.org/games/kbounce.git
- https://invent.kde.org/games/kollision.git
- https://invent.kde.org/games/ksudoku.git
- https://invent.kde.org/games/ktuberling.git
- https://invent.kde.org/games/picmi.git
- https://invent.kde.org/games/klines.git

Keywords: Linux; Android; Game; ARM; Qt; C++; QML; Qt Quick;

Description:

> KDE applications can run on desktop and mobile devices. Very few
> applications have been ported to work on mobile phones though, see
> https://apps.kde.org/platforms/android/ for some examples. Choose
> one of the KDE games and port it to also work on a mobile
> phone. Example KDE applications that work on a mobile phone are
> GCompris and KTuberling. Gcompris use QtQuick and QML to make
> contributions easier.

KDE Games and educational games are integral parts of the KDE Gear
suite. Very few application are currently operational on platforms
other than Linux, such as Android. The supported applications can be
viewed at https://apps.kde.org/platforms/android. Consequently, we
plan to port some of these games to Android, specifically supporting
architectures like arm64-v8a, x86, and x86_64. The ultimate goal is to
distribute their Android versions through apps.kde.org, F-Droid,
Google Store, etc.

Candidates for porting include Kajongg, KMahjongg, KShisen, Kigo,
Blinken, Klickety, Skladnik, Kbounce, Kollision, KSudoku, KTurtle,
Picmi, and Klines. You can learn about their features and code at
apps.kde.org or invent.kde.org.

Finally, you will port one or more of these games to Android and
facilitate community efforts to distribute them through software
stores.

Note:

You need to first read https://community.kde.org/OSPP to understand
how to collaborate with KDE community developers.

Required result:

- Port at least one game to Android.

Knowledge Prerequisite:

- Knowledge of Qt, QML, and Qt Quick.
- Knowledge of Android Packaging.

Mentor:

Benson Muite <benson_muite at emailplus dot org>


## Automated tool of the Hindi glossary

Name: Automated tool of the Hindi glossary

Repository:

- https://invent.kde.org/sysadmin/l10n-scripty
- svn://anonsvn.kde.org/home/kde

Keywords: L10N; Python; Script; Translate;

Description:

The KDE Hindi Group has proposed the creation of an automated tool for
generating and maintaining the glossary for Hindi. This tool will
analyze the word frequency in specified files and generate translation
word lists for various language translation projects. The primary
focus is on extracting English words and translating them into Hindi,
but the tool should be adaptable for other languages as well. It aims
to assist KDE in the automated updating of all Hindi glossary by
extracting appropriate words. Your main tasks will involve tokenizing
and extracting data from Hindi language corpora, designing ranking
algorithms, and automatically generating gettext PO files.

Note:

You need to first read https://community.kde.org/OSPP to understand
how to collaborate with KDE community developers.

This project does not require you use Hindi, but it requires you to
engage extensively with Hindi users and developers within the KDE
community.

Required result:

- Python (or other programming language) script of automated tool.
- Glossary PO of Hindi.

Knowledge Prerequisite:

- Knowledge of Python, Bash.
- Knowledge of Gettext PO.
- Knowledge of basic NLP (grammatical tagging, word sense disambiguation, etc).

Mentor:
